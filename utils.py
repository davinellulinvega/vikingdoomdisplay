#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from enum import Enum


class ItemType(Enum):
    PURSE = 'purse'
    CHEST = 'chest'
    POTION = 'potion'


class EnemyType(Enum):
    SKELETON = 'skeleton'
    TROLL = 'troll'
    DRAGON = 'dragon'


