#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from pyglet.sprite import Sprite
from pyglet.resource import image
from settings import ObsCfg, TILE_SIZE


class Obstacle(Sprite):
    """
    A simple obstacle sprite, representing a static and non-walkable tile in
    the environment.
    """

    def __init__(self, x=0, y=0, batch=None, group=None):
        """
        Simply initialize the parent class, providing it with all the
        necessary elements.
        :param x: Int. The obstacle's position along the X axis.
        :param y: Int. The obstacle's position along the Y axis.
        :param batch: Batch. An instance of the pyglet.graphics.Batch class,
        used to efficiently draw objects in batch rather than sequentially.
        :param group: Group or OrderedGroup. An instance of the Group or
        OrderedGroup class, defining which sprites should be drawn together
        and which sprites overlap which.
        """

        # Load and initialize the image
        obs_img = image('obstacle.png')
        obs_img.anchor_x = obs_img.width // 2
        obs_img.anchor_y = obs_img.height // 2

        # Initialize the parent class
        super(Obstacle, self).__init__(img=obs_img, x=(x + 0.5) * TILE_SIZE,
                                       y=(y + 0.5) * TILE_SIZE, batch=batch,
                                       group=group, usage='static')

        # Scale the sprite to be the right size
        self.scale_x = TILE_SIZE / obs_img.width
        self.scale_y = TILE_SIZE / obs_img.height
