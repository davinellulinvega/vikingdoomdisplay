#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
import os
from logging import getLogger
from logging.config import dictConfig

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
IMAGE_DIR = os.path.join(BASE_DIR, 'Images')
# If the Images directory does not exists already, just create it
if not os.path.isdir(IMAGE_DIR):
    os.mkdir(IMAGE_DIR, mode=0o766)

LOG_DIR = os.path.join(BASE_DIR, 'Logs')
# If the Logs directory does not exists already, just create it
if not os.path.isdir(LOG_DIR):
    os.mkdir(LOG_DIR, mode=0o766)

# Configure the logging utility
dictConfig({
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'simple': {
            'format': "%(asctime)s %(filename)s - %(levelname)s in "
                      "%(module)s.%(funcName)s at line %(lineno)d: %(message)s"
        },
        'compact': {
            'format': "%(asctime)s - %(levelname)s: %(message)s"
        }
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'server': {
            'level': 'WARN',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, "server.log"),
            'formatter': 'compact'
        },
        'client': {
            'level': 'WARN',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, "client.log"),
            'formatter': 'compact'
        },
        'environment': {
            'level': 'WARN',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, "environment.log"),
            'formatter': 'compact'
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'INFO'
        },
        'server': {
            'handlers': ['server', 'console'],
            'level': 'INFO',
            'propagate': False,
        },
        'client': {
            'handlers': ['client', 'console'],
            'level': 'INFO',
            'propagate': False,
        },
        'display': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': False,
        },
        'environment': {
            'handlers': ['environment', 'console'],
            'level': 'INFO',
            'propagate': False,
        },
        'objects': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': False,
        },
    },
})

# Declare some common global constants
NB_TILES = 10
TILE_SIZE = 140


class SrvCfg(object):
    HOST = '0.0.0.0'
    PORT = 8080
    LOG = getLogger('server')


class CltCfg(object):
    LOG = getLogger('client')


class DispCfg(object):
    LOG = getLogger('display')
    VSYNC = False
    FPS_DISPLAY = True
    WIN_HEIGHT = NB_TILES * TILE_SIZE
    if FPS_DISPLAY:
        WIN_WIDTH = (NB_TILES * (TILE_SIZE + 1)) + TILE_SIZE + 70
    else:
        WIN_WIDTH = NB_TILES * (TILE_SIZE + 1)
    UPDATE_RATE = 1 / 144.


class EnvCfg(object):
    LOG = getLogger('environment')


class HeroCfg(object):
    LOG = getLogger('objects')


class EnemyCfg(object):
    LOG = getLogger('objects')


class ItemCfg(object):
    LOG = getLogger('objects')


class ObsCfg(object):
    LOG = getLogger('objects')
