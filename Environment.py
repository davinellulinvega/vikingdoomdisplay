#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from queue import Empty
from pyglet.graphics import Batch, OrderedGroup
from pyglet.sprite import Sprite
from pyglet.resource import image
from Obstacle import Obstacle
from Hero import Hero
from Item import Item
from Enemy import Enemy
from settings import EnvCfg


class Environment(object):
    """
    A container for the different objects present within the environment and
    therefore the game. It also manages those objects, adding and deleting
    them dynamically as they appear, or moving them around.
    """

    def __init__(self, q_in, win_width, win_height):
        """
        Initialize different dictionaries for storing the objects present in
        the environment.
        :param q_in: Queue. An instance of the multiprocess.Queue class, used
        for communicating with the WS client process.
        :param win_width: Int. An integer representing the width of the window.
        :param win_height: Int. An integer representing the height of the
        window.
        """

        # Initialize the parent class
        super(Environment, self).__init__()

        # Store the communication queue
        self._queue = q_in

        # Store the batch for later use
        self._batch = Batch()

        # Declare a background sprite with a grid-like pattern
        self._background = Sprite(img=image('background.png'), x=0, y=0,
                                  group=OrderedGroup(0), batch=self._batch,
                                  usage='static')

        # Declare a group to order the background from the foreground
        self._fg_grp = OrderedGroup(1)

        # Declare a hammer sprite which serves as a waiting screen
        overlay_grp = OrderedGroup(2)
        self._make_overlay('hammer', overlay_grp, win_width, win_height)

        # Declare a skull sprite which serves as a disconnect screen
        self._make_overlay('skull', overlay_grp, win_width, win_height)
        self._skull.visible = False

        # Initialize the different dictionaries used for storing the
        # different objects
        # Objects are referenced by their IDs
        self._heroes = {}
        self._enemies = {}
        self._items = {}
        self._obstacles = []  # Obstacles are just positions, no IDs

    def update(self, dt):
        """
        Update the current content of the environment with the data provided
        within the dictionary in parameter.
        :param dt: A float representing the time that elapsed since the last
        update. It is ignored here, but is required by Pyglet.
        :return: Nothing.
        """

        # Check what's in the queue
        try:
            env_dict = self._queue.get(block=False)
        except Empty:
            return

        # If the result is None, it means that the game is over, so reset the
        # environment
        if env_dict is None:
            self._reset()
            return
        # If the result is -1, it means that the client got disconnected,
        # so reset the environment and display a skull
        elif env_dict == -1:
            self._reset()
            self._dead()
            return

        # Make sure the hammer and skull are not displayed
        self._hammer.visible = False
        self._skull.visible = False

        # Check if obstacles should be initialized or not
        obs_pos = env_dict.get('obstacles')
        if not self._obstacles and obs_pos is not None:
            EnvCfg.LOG.info('Update the list of obstacles: ...')
            # Do the thing
            for obs_pos in obs_pos:
                self._add_obstacle(obs_pos)
            EnvCfg.LOG.info('Done.')

        # Remove Hero, Enemy and Items as necessary
        EnvCfg.LOG.info('Delete any dead hero ...')
        self._delete_hero(set(self._heroes.keys()).difference(env_dict['heroes'].keys()))
        EnvCfg.LOG.info('Done.')
        EnvCfg.LOG.info('Delete any dead enemy ...')
        self._delete_enemy(set(self._enemies.keys()).difference(env_dict['enemies'].keys()))
        EnvCfg.LOG.info('Done.')
        EnvCfg.LOG.info('Delete any gathered item ...')
        self._delete_item(set(self._items.keys()).difference(env_dict['items'].keys()))
        EnvCfg.LOG.info('Done.')

        # Update existing object and add the ones that just appeared
        for e_id, e in env_dict.get('enemies').items():
            if e_id in self._enemies:
                EnvCfg.LOG.info('Update existing enemy with ID: {}'.format(e_id))
                self._enemies[e_id].move(e)
                EnvCfg.LOG.info('Done.')
            else:
                EnvCfg.LOG.info('Add enemy with ID: {}'.format(e_id))
                self._add_enemy(e)
                EnvCfg.LOG.info('Done.')

        for h_id, h in env_dict.get('heroes').items():
            if h_id in self._heroes:
                EnvCfg.LOG.info('Update existing hero with ID: {}'.format(h_id))
                self._heroes[h_id].move(h)
                EnvCfg.LOG.info('Done.')
            else:
                EnvCfg.LOG.info('Add hero with ID: {}'.format(h_id))
                self._add_hero(h)
                EnvCfg.LOG.info('Done.')

        for i_id, i in env_dict.get('items').items():
            if i_id not in self._items:
                EnvCfg.LOG.info('Add item with ID: {}'.format(i_id))
                self._add_item(i)
                EnvCfg.LOG.info('Done.')

    def _make_overlay(self, name, grp, win_width, win_height):
        """
        Declare a sprite that will be used as an overlay and set it as an
        attribute.
        :param name: Str. The name of the overlay and attribute.
        :param grp: OrderedGroup. An instance of the Group or OrderedGroup
        class, giving an order for the rendering.
        :param win_width: Int. An integer representing the width of the window.
        :param win_height: Int. An integer representing the height of the
        :return: Nothing.
        """

        # Load the image and set its anchor point
        img = image('{}.png'.format(name))
        img.anchor_x = img.width // 2
        img.anchor_y = img.height // 2

        # Set the attribute's value
        attr_name = '_{}'.format(name)
        setattr(self, attr_name, Sprite(img=img, x=win_width // 2,
                                        y=win_height // 2, group=grp,
                                        batch=self._batch, usage='static'))

        # Scale the sprite
        attr = getattr(self, attr_name)
        attr.scale_x = win_width / (4 * img.width)
        attr.scale_y = win_height / (4 * img.height)

    def _reset(self):
        """
        A reset all the class attributes.
        :return: Nothing.
        """

        # Do not display the skull
        self._skull.visible = False

        # Display the hammer
        self._hammer.visible = True

        # Delete all the obstacles
        for obs in self._obstacles:
            obs.delete()
        self._obstacles = []

        # Delete any remaining hero
        self._delete_hero(list(self._heroes.keys()))

        # Delete any remaining enemy
        self._delete_enemy(list(self._enemies.keys()))

        # Delete any remaining item
        self._delete_item(list(self._items.keys()))

        # Re-initialize all the attributes
        self._heroes = {}
        self._enemies = {}
        self._items = {}
        self._obstacles = []

    def _dead(self):
        """
        Display a skull in place of the hammer, whenever the client gets
        disconnected.
        :return: Nothing.
        """

        # Make sure the hammer is not displayed
        self._hammer.visible = False

        # Make sure the skull is displayed
        self._skull.visible = True

    def _add_obstacle(self, pos):
        """
        Add a sprite representing an obstacle within the environment at the
        given position.
        :param pos: Tuple. A tuple representing the obstacle's position along
        the X and Y axis.
        :return: Nothing.
        """

        EnvCfg.LOG.info("Add obstacle at position: {}".format(pos))

        # Add a new obstacle sprite to the list
        obs = Obstacle(x=pos[0], y=pos[1], batch=self._batch,
                       group=self._fg_grp)
        self._obstacles.append(obs)

    def _add_hero(self, hero):
        """
        Add a hero sprite to the environment.
        :param hero: Dict. A dictionary describing a hero.
        :return: Nothing.
        """

        EnvCfg.LOG.info('Adding hero: {}'.format(hero))

        # Instantiate a new hero and add it to the dictionary of existing heroes
        self._heroes[str(hero.get('id'))] = Hero(hero, batch=self._batch,
                                                 group=self._fg_grp)

    def _add_enemy(self, enemy):
        """
        Add an enemy sprite to the environment.
        :param enemy: Dict. A dictionary representing an enemy.
        :return: Nothing.
        """

        # Instantiate a new enemy and add it to the dictionary of existing
        # enemies
        self._enemies[str(enemy.get('id'))] = Enemy(enemy, batch=self._batch,
                                                    group=self._fg_grp)

    def _add_item(self, item):
        """
        Add an item sprite to the environment.
        :param item: Dict. A dictionary representing a given item.
        :return: Nothing.
        """

        # Instantiate a new item and add it to the dictionary of existing items
        self._items[str(item.get('id'))] = Item(item, batch=self._batch,
                                                group=self._fg_grp)

    def _delete_hero(self, ids):
        """
        Given a list of hero IDs, remove the corresponding sprites.
        :param ids: List. A list of IDs corresponding to the heroes to remove.
        :return: Nothing.
        """

        # Let the user know which heroes are dead
        EnvCfg.LOG.info('Removing hero(es) with the ID(s): {}'.format(ids))

        # Remove them heroes
        for hero_id in ids:
            self._heroes[hero_id].delete()
            del self._heroes[hero_id]

    def _delete_enemy(self, ids):
        """
        Given a list of enemy IDs, remove the corresponding sprites.
        :param ids: List. A list of IDs corresponding to the enemies to remove.
        :return: Nothing.
        """

        # Let the user know which enemies are dead
        EnvCfg.LOG.info('Removing enemy(ies) with the ID(s): {}'.format(ids))

        # Remove them enemies
        for enemy_id in ids:
            self._enemies[enemy_id].delete()
            del self._enemies[enemy_id]

    def _delete_item(self, ids):
        """
        Given a list of item IDs, remove the corresponding sprites.
        :param ids: List. A list of IDs corresponding to the item to remove.
        :return: Nothing.
        """

        # Let the user know which items are dead
        EnvCfg.LOG.info('Removing item(s) with the ID(s): {}'.format(ids))

        # Remove them items
        for item_id in ids:
            self._items[item_id].delete()
            del self._items[item_id]

    @property
    def batch(self):
        return self._batch
