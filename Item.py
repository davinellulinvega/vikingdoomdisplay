#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from pyglet.sprite import Sprite
from pyglet.resource import image
from utils import ItemType
from settings import ItemCfg, TILE_SIZE


class Item(Sprite):
    """
    A container and sprite representing an Item in the environment.
    """

    def __init__(self, item, batch=None, group=None):
        """
        Initialize the sprite's image and location in the environment.
        :param item: Dict. A dictionary containing the description of a given
        item.
        :param batch: Batch. An instance of the pyglet.graphics.Batch class,
        used to efficiently draw objects in batch rather than sequentially.
        :param group: Group or OrderedGroup. An instance of the Group or
        OrderedGroup class, defining which sprites should be drawn together
        and which sprites overlap which.
        """

        # Get the hero's position
        x, y = item.get('position')

        # Get the image based on the Item type
        item_type = ItemType(item.get('type'))
        if item_type == ItemType.PURSE:
            img = image('purse.png')
        elif item_type == ItemType.CHEST:
            img = image('chest.png')
        elif item_type == ItemType.POTION:
            img = image('potion.png')

        # Change the image's anchor point to be in its middle
        img.anchor_x = img.width // 2
        img.anchor_y = img.height // 2

        # Initialize the parent class
        super(Item, self).__init__(img=img, x=(x + 0.5) * TILE_SIZE,
                                   y=(y + 0.5) * TILE_SIZE, batch=batch,
                                   group=group, usage='static')

        # Scale the sprite to be the right size
        self.scale_x = TILE_SIZE / img.width
        self.scale_y = TILE_SIZE / img.height
