#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from pyglet.sprite import Sprite
from pyglet.resource import image
from settings import HeroCfg, TILE_SIZE


class Hero(Sprite):
    """
    A container and sprite representing a hero in the environment.
    """

    def __init__(self, hero, batch=None, group=None):
        """
        Initialize the sprite's image and location in the environment.
        :param hero: Dict. A dictionary containing the description of a given
        hero.
        :param batch: Batch. An instance of the pyglet.graphics.Batch class,
        used to efficiently draw objects in batch rather than sequentially.
        :param group: Group or OrderedGroup. An instance of the Group or
        OrderedGroup class, defining which sprites should be drawn together
        and which sprites overlap which.
        """

        # Get the hero's position
        x, y = hero.get('position')

        # Load and initialize the image
        hero_img = image('hero.png')
        hero_img.anchor_x = hero_img.width // 2
        hero_img.anchor_y = hero_img.height // 2

        # Initialize the parent class
        super(Hero, self).__init__(img=hero_img, x=(x + 0.5) * TILE_SIZE,
                                   y=(y + 0.5) * TILE_SIZE, batch=batch,
                                   group=group)

        # Scale the sprite to be the right size
        self.scale_x = TILE_SIZE / hero_img.width
        self.scale_y = TILE_SIZE / hero_img.height

    def move(self, hero):
        """
        Update the sprite's position, based on the given hero's one.
        :param hero: Dict. A dictionary containing the description of a given
        hero.
        :return: Nothing
        """

        # Get the hero's position
        x, y = hero.get('position')

        # Update the sprite's position
        self.update(x=(x + 0.5) * TILE_SIZE, y=(y + 0.5) * TILE_SIZE)
