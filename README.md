# Description
This project can be considered a sub-module of the [Viking Doom project](https://gitlab.com/davinellulinvega/vikingdoomv2). The goal, here, is to provide a windowed display in which to render the game's state in, or near, real-time. Since Viking Doom is meant to be run headless on High Performance Computers (HPC), a remote display allows for debugging and monitoring of the game and the different controllers. It is built around a client/server architecture using the websocket protocol. The game engine plays the role of the server broadcasting data about the current state of the game and the heroes involved, while the display, implemented in this project, acts as a client. From the received data the position of the different objects (heroes, enemy units, items and obstacles) are extracted and the screen updated to reflect the new state of the game.

# Installation
As is the case for the Viking Doom project, the display has been implemented and tested to run with Python 3.7.6 and later versions.
## Requirements
This project relies on the [PyGlet](https://pyglet.readthedocs.io/en/latest/) for building the window and managing the different objects that live within it, as well as the [SocketIO](https://python-socketio.readthedocs.io/en/latest/) framework for handling the communication between server and clients.

A detailed list of the requirements, along with their dependencies, for running this project is available in the [requirements.txt](requirements.txt) file. The important libraries being:
```
aiohttp (3.6.2)
pyglet (1.4.10)
python-engineio (3.11.2)
python-socketio (4.4.0)
requests (2.22.0)
```

## With PIP
All the packages necessary for executing both the client and server scripts can be installed via pip with the command:
```bash
> pip install -U -r requirements.txt
```
or
```bash
> pip install --user -U -r requirements.txt
```
Depending on whether you want to install the packages system-wide or for the current user only.

# Using the display
Although, it has been said above that the game engine acts as the server, it actually implements a websocket client that only sends data to the `server.py` script. Consequently, to use the display you need to first start the communication hub by running the following command:
```bash
> python server.py
```
The hostname, as well as the port on which the server listens for incoming connections can be configured in the `[settings.py](settings.py)` file.

Once the server has finished it starting sequence without errors, the display can be opened by entering the following command, in a separate terminal:
```bash
> python client.py
```
By default the client will use the same hostname and port as the server. However, those can be specified on the command line by providing the appropriate values to the `--host` and `--port` parameters. For more details on the client's parameters, please look at `python client.py --help`.
