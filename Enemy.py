#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from pyglet.sprite import Sprite
from pyglet.resource import image
from utils import EnemyType
from settings import EnemyCfg, TILE_SIZE


class Enemy(Sprite):
    """
    A container and sprite representing an enemy in the environment.
    """

    def __init__(self, enemy, batch=None, group=None):
        """
        Initialize the sprite's image and location in the environment.
        :param enemy: Dict. A dictionary containing the description of a given
        enemy.
        :param batch: Batch. An instance of the pyglet.graphics.Batch class,
        used to efficiently draw objects in batch rather than sequentially.
        :param group: Group or OrderedGroup. An instance of the Group or
        OrderedGroup class, defining which sprites should be drawn together
        and which sprites overlap which.
        """

        # Get the hero's position
        x, y = enemy.get('position')

        # Get the image based on the Enemy type
        enemy_type = EnemyType(enemy.get('type'))
        if enemy_type == EnemyType.SKELETON:
            img = image('skeleton.png')
        elif enemy_type == EnemyType.TROLL:
            img = image('troll.png')
        elif enemy_type == EnemyType.DRAGON:
            img = image('dragon.png')

        # Change the anchor to be in the middle of the image
        img.anchor_x = img.width // 2
        img.anchor_y = img.height // 2

        # Initialize the parent class
        super(Enemy, self).__init__(img=img, x=(x + 0.5) * TILE_SIZE,
                                    y=(y + 0.5) * TILE_SIZE, batch=batch,
                                    group=group)

        # Scale the sprite to be the right size
        self.scale_x = TILE_SIZE / img.width
        self.scale_y = TILE_SIZE / img.height

    def move(self, enemy):
        """
        Update the sprite's position, based on the given enemy's one.
        :param enemy: Dict. A dictionary containing the description of a given
        enemy.
        :return: Nothing
        """

        # Only move if the enemy is not static
        if not enemy.get('is_static'):
            # Get the hero's position
            x, y = enemy.get('position')

            # Update the sprite's position
            self.update(x=(x + 0.5) * TILE_SIZE, y=(y + 0.5) * TILE_SIZE)
