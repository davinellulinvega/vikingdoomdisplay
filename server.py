#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from socketio import AsyncServer
from aiohttp import web
from settings import SrvCfg

# Instantiate the server
server = AsyncServer(async_mode='aiohttp')

# Define some event handlers
@server.on('connect')
def on_connect(sid, environ):
    """
    Simply write a line in the logs to keep track of connected users.
    :param sid: Int. An integer representing the unique identifier attributed
    to the connected user.
    :param environ: Dict. A dictionary containing information regarding the
    new client, its context and session.
    :return: Nothing.
    """

    # Simply write some log
    SrvCfg.LOG.info('New client connected: {}'.format(sid))


@server.on('disconnect')
async def on_disconnect(sid):
    """
    Simply write a line in the logs to keep track of the disconnected users.
    :param sid: Int. An integer representing the unique identifier attributed
    to the connected user.
    :return: Nothing.
    """
    global server

    # Write to the logs
    SrvCfg.LOG.info('Client disconnected: {}'.format(sid))

    # Let the other know that we might be in a game over state
    await server.emit('game_over', data=None, skip_sid=sid)


@server.on('update')
async def on_update(sid, data):
    """
    Triggered each time the game environment is updated and, therefore,
    needs to be rendered again.
    :param sid: Int. An integer representing the unique identifier attributed
    to the connected user.
    :param data: Dict. A dictionary whose content describes the current state
    of the environment.
    :return: Nothing.
    """
    global server

    # Warn the user that we got an update event
    # SrvCfg.LOG.info('Got an UPDATE Event with data: {}'.format(data))
    SrvCfg.LOG.info('Got an UPDATE Event.')

    # Broadcast the received data to all connected clients, but the one
    # which triggered the event in the first place
    SrvCfg.LOG.info('Re-emitting the UPDATE event to all connected clients, '
                    'but for client with SID: {}'.format(sid))
    await server.emit('update', data=data, skip_sid=sid)


@server.on('game_over')
async def on_game_over(sid):
    """
    Warn all the client that the game has ended.
    :param sid: Int. An integer representing the unique identifier attributed
    to the connected user.
    :return: Nothing.
    """
    global server

    # Simply broadcast the game over information to all the client but the
    # one doing the sending
    await server.emit('game_over', data=None, skip_sid=sid)


# Instantiate an asynchronous application
app = web.Application(logger=SrvCfg.LOG)

# Attach the server to the application
server.attach(app)

if __name__ == "__main__":
    # /!\ reuse_port does not work on windows
    web.run_app(app, host=SrvCfg.HOST, port=SrvCfg.PORT,
                reuse_port=True, access_log=None)
