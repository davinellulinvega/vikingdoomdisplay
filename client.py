#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
import argparse
from multiprocessing import Queue
from socketio import Client
from socketio.exceptions import ConnectionError
from Display import Display
from settings import CltCfg, SrvCfg

# Instantiate a websocket client
clt = Client()

# Declare a queue for communicating with the environment
Q_OUT = Queue()

# Instantiate a display
display = Display(Q_OUT)

# Define some event callbacks
@clt.on('update')
def on_update(data):
    """
    Update the state of the environment, using the data provided in parameter.
    :param data: Dict. A dictionary describing the current state of the
    environment, as well as the different objects, part of said environment.
    :return: Nothing.
    """
    global display

    # Warn the user that an update event has been received
    CltCfg.LOG.info('Got an UPDATE event ...')

    # Queue the data to let the environment update itself whenever possible
    Q_OUT.put(data)

    # Done processing everything
    CltCfg.LOG.info('Done.')


@clt.on('game_over')
def on_game_over(data):
    """
    Reset the state of the environment, since the game just ended.
    :param data: None.
    :return: Nothing.
    """
    global Q_OUT

    # Put None on the Queue, letting the environment know that the game is over
    # and that it should reset
    Q_OUT.put(None)


@clt.on('connect')
def on_connect():
    """

    :return:
    """
    global Q_OUT

    # Put None on the Queue, letting the environment know that the client is
    # connected again and that it should reset
    Q_OUT.put(None)


@clt.on('disconnect')
def on_disconnect():
    """
    If the client gets suddenly disconnected, let the display know about it.
    :return: Nothing.
    """
    global Q_OUT

    # Put a special value inside the Queue to let the environment know that
    # the client got disconnected
    Q_OUT.put(-1)


if __name__ == "__main__":
    # Define some command line parameters for the host and port of the
    # websocket server
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', dest='host', type=str, default=SrvCfg.HOST,
                        help='The host name or ip address of the websocket '
                             'server.')
    parser.add_argument('--port', dest='port', type=int, default=SrvCfg.PORT,
                        help='The port on which the websocket server is '
                             'listening.')

    # Parse the command line arguments
    args = parser.parse_args()

    try:
        # Start the display process
        CltCfg.LOG.info('Starting the display process ...')
        display.start()
        CltCfg.LOG.info('Done.')

        # Leave a message for the user
        url = 'http://{}:{}/'.format(args.host, args.port)
        CltCfg.LOG.info('Connecting to the websocket server: {}'.format(url))

        # Connect the client to the websocket server
        try:
            clt.connect(url)
        except ConnectionError:
            CltCfg.LOG.exception('An error occurred while trying to connect '
                                 'to the server.')
            display.kill()

        # Leave another set of messages for the user
        CltCfg.LOG.info('Done.')

        # Wait forever
        clt.wait()

    except KeyboardInterrupt:
        # Some final message
        CltCfg.LOG.info('Exiting the main program ...')

    finally:
        # Disconnect from the server
        CltCfg.LOG.info('Disconnecting to the websocket server.')
        clt.disconnect()

        # Empty the output queue
        CltCfg.LOG.info('Emptying the output queue ...')
        while not Q_OUT.empty():
            Q_OUT.get()
        CltCfg.LOG.info('Done.')

        # Wait for the display process to finish
        CltCfg.LOG.info('Waiting for the display process to shutdown ...')
        display.join()
        CltCfg.LOG.info('Done.')
