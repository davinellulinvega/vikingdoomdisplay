#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from multiprocessing import Process
from settings import IMAGE_DIR, DispCfg

# TODO: Make the whole display dynamic regarding the computer screen size. So switching between a big and small screen, should update the size of the different sprites, even those already on the map, not just the new ones.


class Display(Process):
    """
    Defines a process for managing the whole display (window, event loop and
    environment). This avoids having many event loops and threads together
    with the Websocket client. It also avoids having a GLX context spread
    across and accessed from different threads.
    """

    def __init__(self, env_q_in):
        """
        Initialize the parent class and declare empty attributes that will be
        filled later on.
        :param env_q_in: Queue. An instance of the multiprocess.Queue class,
        used for communicating between the WS client process and the
        environment.
        """

        # Initialize the parent class
        super(Display, self).__init__()

        # Declare the different required attributes
        self._env_q_in = env_q_in
        # Those are initialized to None, since they will be filled in later.
        # In case anything goes wrong during this process, a None value is
        # sure to raise an exception, sooner rather than later
        self._win = None
        self._evt_loop = None
        self._env = None

    def _init(self):
        """
        This is where  the real initialization of the class' attributes takes
        place and the main modules are imported. This is to avoid sharing
        context and variables with the caller.
        :return: Nothing.
        """

        # Import the required pyglet modules
        from pyglet import resource
        from pyglet.window import Window, FPSDisplay
        from pyglet.app import EventLoop
        from pyglet.text import Label
        from Environment import Environment

        # Declare the resource location
        resource.path = [IMAGE_DIR]
        resource.reindex()

        # Get the event loop
        self._evt_loop = EventLoop()

        # Define a new window
        self._win = Window(width=DispCfg.WIN_WIDTH,
                           height=DispCfg.WIN_HEIGHT, resizable=False,
                           fullscreen=False, vsync=DispCfg.VSYNC)

        # Define the handler for the window's on_draw and on_key_press events
        self._win.on_draw = self._on_draw
        self._win.on_key_press = self._on_shutdown

        # Declare a new environment
        self._env = Environment(self._env_q_in, self._win.width,
                                self._win.height)

        # If required declare an FPS Display
        if DispCfg.FPS_DISPLAY:
            self._fps = FPSDisplay(self._win)
            self._fps.label = Label(font_size=14, bold=True,
                                    x=DispCfg.WIN_WIDTH - 65,
                                    y=5,
                                    color=(255, 255, 255, 255),
                                    batch=self._env.batch)
        else:
            self._fps = None

    def _on_draw(self):
        """
        Called whenever the window needs to be redrawn. This method, simply
        clears the current window and draws the sprites that are within the
        environment's batch.
        :return: Nothing.
        """

        # Clear the window
        self._win.clear()

        # Draw the sprites in batch
        self._env.batch.draw()

    def _on_shutdown(self, symbol, modifiers):
        """
        Called whenever the user presses either Q or ESC. Closes the window,
        shut down the event loop and finally let the process end gracefully.
        :return: Nothing.
        """

        # Import the required modules
        from pyglet.clock import unschedule
        from pyglet.window import key

        if symbol == key.Q or symbol == key.ESCAPE:
            # Unschedule the environment's update
            unschedule(self._env.update)

            # Close the window
            self._win.close()

            # Exit out of the event loop
            self._evt_loop.exit()

            # Close the communication queue
            self._env_q_in.close()

            # Close the current process
            self.close()

    def run(self) -> None:
        """
        Schedule for the environment to be updated as frequently as possible
        and then run the event loop.
        :return: Nothing.
        """

        # Again import the required clock module
        from pyglet.clock import schedule_interval_soft

        # Actually initialize the display
        self._init()

        # Schedule for the environment to update as fast as possible
        schedule_interval_soft(self._env.update, DispCfg.UPDATE_RATE)

        # Run the event loop
        self._evt_loop.run()
